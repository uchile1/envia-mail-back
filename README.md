# ENVIA-MAIL-BACK

Componente backend transversal para el envío de correos electrónicos

# Requisitos para levantar local

- <a href="https://golang.org/" target="_blank">Golang 1.17 o superior</a>
- <a href="https://www.docker.com/" target="_blank">Docker</a>
- <a href="https://www.gnu.org/software/make/" target="_blank">GNU Make</a>


## Instrucciones para levantar el proyecto

1. Obtener el proyecto del repositorio

    ```sh
    git clone https://gitlab02.uchile.cl/desarrollo-vti/envia-mail-back.git
    ```

2. Ingresar a la carpeta donde se ubica el proyecto

    ```sh
    cd envia-mail-back
    ```

3. Construir imagen Docker

    ```sh
    make docker-build
    ```

4. Correr un contenedor en base a la imagen construida:

    ```sh
    make docker-shell
    ```

5. Dentro del contenedor ejecutar

    ```sh
    make go
    ```

    >El output del terminal debiese ser:
    >
    >```
    >Listening and serving HTTP on :8080
    >```
    >


# Cómo  contribuir?  :muscle:


* Para agregar alguna mejora o funcionalidad:

    ```sh
    git clone https://gitlab02.uchile.cl/desarrollo-vti/envia-mail-back.git && cd envia-mail-back
    ```

* Descargar dependencias:

    ```sh
    go mod tidy
    ```

* Subir al repositorio los cambios realizados, con las siguientes instrucciones:

    ```sh
    git  commit  -am 'new version: explicar el aporte'

    git  push  origin master
    ```

# Subir nueva versión al repositorio privado

* Modificar las siguientes variables en el archivo *Makefile* que está en la raíz del proyecto, si corresponde, para modificar el registry privado y la versión del artefacto:
    
    REGISTRY=nexus3.uchile.cl:8087/desarrollo-vti/
    
    VERSION=0.0.0-RELEASE

* Ahora para subir la imagen docker al registry:

    ```sh
    make docker-push
    ```

**IMPORTANTE**:

Mantener actualizados el Readme y Wiki si estás realizando alguna modificación, nueva funcionalidad o cambio de tecnología.

Así quién posteriormente perdido en el espacio como tú necesite saber o conocer de este componente, pueda contar siempre con la información lo más actualizada posible.

<img src="https://media.giphy.com/media/UKWxGMEPjRwCA/giphy-downsized.gif" width="500">



