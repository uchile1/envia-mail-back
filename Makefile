SHELL=/bin/sh
PACKAGE_NAME=email-api-back
PROJECT_FOLDER=.
GIT_DIR=$(shell pwd)
REGISTRY=nexus32.uchile.cl:8085/desarrollo-vti/
VERSION=0.0.1-SNAPSHOT

#run:
#	source .env
#	go run cmd/main.go

docker-build:
	docker build -t $(PACKAGE_NAME)-dev -f Dockerfile.dev  $(GIT_DIR)

docker-shell:
	docker run -it --rm --env-file=.env -v $(GIT_DIR):/app --net host -w /app/$(PROJECT_FOLDER) --entrypoint=/bin/sh $(PACKAGE_NAME)-dev	


docker-build-release:
	docker build -t $(REGISTRY)$(PACKAGE_NAME):$(VERSION) -f Dockerfile  $(GIT_DIR)

docker-push: docker-build-release
	docker image push $(REGISTRY)$(PACKAGE_NAME):$(VERSION)

go:	
	go run cmd/main.go	