package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	email "github.com/xhit/go-simple-mail/v2"
	"gitlab02.uchile.cl/desarrollo-vti/helper-common/helperCommon"
	"gitlab02.uchile.cl/go-utils/check"
	"go.elastic.co/apm"
)

type Email struct {
	Subject  string     `json:"asunto,omitempty"`
	Message  string     `json:"contenido,omitempty"`
	To       []string   `json:"to,omitempty"`
	Cc       *[]string  `json:"cc,omitempty"`
	Bcc      *[]string  `json:"bcc,omitempty"`
	Adjuntos *[]Adjunto `json:"adjuntos,omitempty"`
}

type Adjunto struct {
	File []byte `json:"archivo,omitempty"`
	Name string `json:"nombre_archivo,omitempty"`
}

// Método para enviar correo
// @ID           Send
// @Summary      método para enviar correo.
// @Description  método para enviar correo.
// @Tags         Send
// @Accept       mpfd
// @Produce      json
// @Param        jsonByte  header  []string  true "[]byte que es un json codifificado en base64"
// @Success      200  {object}  map[string]string
// @Failure      400  {object}  map[string]string
// @Failure      500  {object}  map[string]string
// @Router       /api/v1/notificacion [post]
func Send(c *gin.Context) {
	span, ctx := apm.StartSpan(c, helperCommon.GetFrame(1).Function, "func")
	var err error
	var body Email
	statusCode := http.StatusOK
	message := "correo enviado exitosamente"
	defer func() {
		errString := ""
		if err != nil {
			apm.CaptureError(ctx, err).Send()
			errString = err.Error()
		} else {
			log.Infof("[DEFER Send] correos enviados a -> to: %v, cc: %v, bcc: %v", body.To, body.Cc, body.Bcc)
		}
		span.End()
		c.JSON(statusCode, gin.H{
			"message": message,
			"error":   errString,
		})

	}()
	file, err := c.FormFile("jsonByte")
	if err != nil {
		statusCode = http.StatusBadRequest
		message = "error al obtener archivo adjunto"
		log.Errorf("<<<ERROR: %s --> %v", message, err)
		return
	}
	fileContent, _ := file.Open()
	byteContainer, err := ioutil.ReadAll(fileContent)
	if err != nil {
		statusCode = http.StatusBadRequest
		message = "error al ler contenido del body del request"
		log.Errorf("<<<ERROR: %s --> %v", message, err)
		return
	}
	err = json.Unmarshal(byteContainer, &body)
	if err != nil {
		statusCode = http.StatusBadRequest
		message = "error al parsear body del request"
		log.Errorf("<<<ERROR: %s --> %v", message, err)
		return
	}
	msgAux, errAux := validateRequest(body)
	if errAux != nil {
		statusCode = http.StatusBadRequest
		message = msgAux
		err = errAux
		return
	}

	server := email.NewSMTPClient()
	server.Host = check.Check("SERVER", "SERVER missing!")
	server.Port = 25
	server.Encryption = email.EncryptionSTARTTLS
	server.KeepAlive = false
	// Timeout for connect to SMTP Server
	server.ConnectTimeout = 30 * time.Second
	server.Authentication = email.AuthNone

	// Timeout for send the data and wait respond
	timeout, err := strconv.Atoi(check.Check("TIMEOUT_SECONDS", "TIMEOUT_SECONDS missing!"))
	if err != nil {
		statusCode = http.StatusInternalServerError
		message = "error al parsear TIMEOUT_SECONDS"
		log.Errorf("<<<ERROR: %s --> %v", message, err)
		return
	}
	server.SendTimeout = time.Duration(timeout) * time.Second
	// SMTP client
	smtpClient, err := server.Connect()
	if err != nil {
		statusCode = http.StatusInternalServerError
		message = "error al conectarse al servidor: " + server.Host
		log.Errorf("<<<ERROR: %s --> %v", message, err)
		return
	}
	msg := email.NewMSG()
	msg.SetFrom(strings.TrimSpace(check.Check("FROM", "FROM missing!"))).AddTo(body.To...).SetSubject(body.Subject)
	if body.Cc != nil {
		msg.AddCc(*body.Cc...)
	}
	if body.Bcc != nil {
		msg.AddBcc(*body.Bcc...)
	}
	msg.SetBody(email.TextHTML, body.Message)
	if body.Adjuntos != nil {
		for _, att := range *body.Adjuntos {
			ext := strings.TrimSuffix(att.Name, filepath.Ext(att.Name))
			msg.AddAttachmentData(att.File, att.Name, ext)
		}

	}

	if msg.Error != nil {
		err = msg.Error
		statusCode = http.StatusBadRequest
		message = "error en la validación antes de enviar correo"
		log.Errorf("<<<ERROR: %s --> %v", message, msg.Error)
		return
	}
	err = msg.Send(smtpClient)
	if err != nil {
		statusCode = http.StatusInternalServerError
		message = "error al enviar correo"
		log.Errorf("<<<ERROR: %s --> %v", message, err)
		return
	}
}

func validateRequest(body Email) (string, error) {
	msjErr := "Parámetro '%s' es obligatorio"
	if body.To == nil {
		msj := fmt.Sprintf(msjErr, "to")
		log.Errorf("<<<ERROR: %s", msj)
		return msj, errors.New(msj)
	}
	if body.Subject == "" {
		msj := fmt.Sprintf(msjErr, "asunto")
		log.Errorf("<<<ERROR: %s", msj)
		return msj, errors.New(msj)
	}
	if body.Message == "" {
		msj := fmt.Sprintf(msjErr, "contenido")
		log.Errorf("<<<ERROR: %s", msj)
		return msj, errors.New(msj)
	}
	return "", nil
}
