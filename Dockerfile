FROM golang:1.18.0-alpine3.15 as builder
ENV GO111MODULE=on
WORKDIR /app
COPY . .

RUN apk add --no-cache git musl-dev gcc

# Git user
RUN git config \
  --global \
  url."https://gitlab02:desarrollo@gitlab02.uchile.cl".insteadOf \
  "https://gitlab02.uchile.cl"
  
RUN go mod tidy && \
    CGO_ENABLED=1 GOOS=linux GOARCH=amd64 go build -o httpserver cmd/*.go
    
# final stage
FROM alpine:3.15.0
ENV SWAGGER_PATH="/app/docs/swagger.json"

# to prevent privilege escalation attacks
RUN addgroup -S app -g 1000 && \
    adduser -S -g app app --uid 1000

RUN apk add tzdata
ENV TZ=America/Santiago

COPY --from=builder --chown=app:app /app/httpserver /app/
COPY --chown=app:app docs/swagger.json $SWAGGER_PATH

USER app

EXPOSE 80
ENTRYPOINT ["/app/httpserver"]
