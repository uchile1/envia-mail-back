package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

const (
	subject     = "Test Unit"
	htmlMessage = `
<!DOCTYPE html>
<html>
<body>

<h1>My First Heading</h1>
<p>My first paragraph.</p>
<a href="https://www.w3schools.com">This is a link</a>
</body>
</html>`
)

// func main() {
// 	utilHttp := helperHttp.New(nil)

// 	e := api.Email{Subject: "este es un asunto", Message: htmlMessage, To: []string{"hugo.carcamo@uchile.cl"},
// 		Cc: &[]string{"hugomode@gmail.com", "test.fake.test@uchile.cl"}, Bcc: &[]string{"fake.fake@uchile.cl"},
// 		Adjuntos: &[]api.Adjunto{api.Adjunto{File: file("fono.txt"), Name: "fono.txt"},
// 			api.Adjunto{File: file("smapa.txt"), Name: "smapa.txt"}}}
// 	bodyBuf, contentType, err := helperCommon.MakeFormFile(e, "jsonByte", "struct")
// 	if err != nil {
// 		fmt.Println(err)
// 		os.Exit(1)
// 	}
// 	utilHttp.SetHeader(http.Header{"Content-Type": {*contentType}})
// 	utilHttp.SetPrintCurl(true)
// 	respuesta, err := utilHttp.PostRest("http://localhost:8080/api/v1/notificacion", bodyBuf, time.Second*90)
// 	if err != nil {
// 		fmt.Println("Error al realizar el request POST EnviaMail: ", err)
// 		os.Exit(1)
// 	}
// 	fmt.Println(string(respuesta.Data))
// }

func file(path string) []byte {
	tryTwoCertBytes, err := ioutil.ReadFile(path)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	return tryTwoCertBytes
}

type person1 struct {
	Username string  `json:"username"`
	Lastname *string `json:"apellido"`
}

func main() {
	ap := "cárcamo"
	printIfperson1(person1{Username: "hugo", Lastname: &ap})
}

func printIfperson1(object interface{}) {
	p, ok := object.(person1)
	if ok {
		fmt.Printf("Hello %s !\n", *p.Lastname)
	}
}
