package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"testing"

	unitTest "github.com/Valiben/gin_unit_test"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"gitlab02.uchile.cl/desarrollo-vti/email-api-back/api"
	"gitlab02.uchile.cl/desarrollo-vti/helper-common/helperCommon"
)

func init() {
	// initialize the router
	r := gin.Default()
	prefix := "/api/v1"
	v1NoAuth := r.Group(prefix)
	v1NoAuth.POST("/notificacion", api.Send)
	// Setup the router
	unitTest.SetRouter(r)
	godotenv.Load("../.env")
}

const (
	subject     = "Test Unit"
	htmlMessage = `
<!DOCTYPE html>
<html>
<body>

<h1>My First Heading</h1>
<p>My first paragraph.</p>
<a href="https://www.w3schools.com">This is a link</a>
</body>
</html>`
)

type ResponseEmail struct {
	Error   string `json:"error,omitempty"`
	Message string `json:"message,omitempty"`
}

func TestEnviaMailSimple(t *testing.T) {
	var resp ResponseEmail
	body := api.Email{Subject: subject + " - " + helperCommon.GetFrame(1).Func.Name(),
		To:      []string{"hugo.carcamo@uchile.cl"},
		Message: htmlMessage}

	tmpFile := getTmpFile(t, body)
	defer os.Remove(tmpFile.Name())
	err := unitTest.TestFileHandlerUnMarshalResp(http.MethodPost, "/api/v1/notificacion", tmpFile.Name(), "jsonByte", nil, &resp)
	if err != nil {
		t.Errorf("<<<ERROR en test: %v", err)
		return
	}
	t.Logf("OK %s", helperCommon.GetFrame(1).Func.Name())
}

func TestEnviaMailCcBcc(t *testing.T) {
	var resp ResponseEmail
	body := api.Email{Subject: subject + " - " + helperCommon.GetFrame(1).Func.Name(),
		To:      []string{"hugo.carcamo@uchile.cl"},
		Bcc:     &[]string{"hugomode@gmail.com"},
		Cc:      &[]string{"hugomode@hotmail.com"},
		Message: htmlMessage}
	tmpFile := getTmpFile(t, body)
	defer os.Remove(tmpFile.Name())
	err := unitTest.TestFileHandlerUnMarshalResp(http.MethodPost, "/api/v1/notificacion", tmpFile.Name(), "jsonByte", nil, &resp)
	if err != nil {
		t.Errorf("<<<ERROR en test: %v", err)
		return
	}
	t.Logf("OK %s", helperCommon.GetFrame(1).Func.Name())
}

func TestEnviaMailAdjunto(t *testing.T) {
	b, err := ioutil.ReadFile(".2016_20_sin_revisar.pdf")
	var resp ResponseEmail
	body := api.Email{Subject: subject + " - " + helperCommon.GetFrame(1).Func.Name(),
		To:       []string{"hugo.carcamo@uchile.cl"},
		Cc:       &[]string{"hugomode@gmail.com"},
		Message:  htmlMessage,
		Adjuntos: &[]api.Adjunto{{File: b, Name: "adjunto.pdf"}},
	}
	tmpFile := getTmpFile(t, body)
	defer os.Remove(tmpFile.Name())
	err = unitTest.TestFileHandlerUnMarshalResp(http.MethodPost, "/api/v1/notificacion", tmpFile.Name(), "jsonByte", nil, &resp)
	if err != nil {
		t.Errorf("<<<ERROR en test: %v", err)
		return
	}
	t.Logf("OK %s", helperCommon.GetFrame(1).Func.Name())
}

func getTmpFile(t *testing.T, body api.Email) *os.File {
	b, err := json.Marshal(body)
	if err != nil {
		t.Fatal("<<<ERROR Marshal en test ", err)
	}
	tmpFile, err := ioutil.TempFile(".", "prefix-")
	if err != nil {
		t.Fatal("Cannot create temporary file", err)
	}
	if _, err = tmpFile.Write(b); err != nil {
		t.Fatal("Failed to write to temporary file", err)
	}

	if err := tmpFile.Close(); err != nil {
		t.Fatal("Failed close temporary file", err)
	}
	return tmpFile
}
