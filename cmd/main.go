package main

import (
	"fmt"
	"os"

	"github.com/gin-gonic/gin"
	sf "github.com/swaggo/files"
	gs "github.com/swaggo/gin-swagger"
	"gitlab02.uchile.cl/desarrollo-vti/email-api-back/api"
	"gitlab02.uchile.cl/desarrollo-vti/email-api-back/docs"
	"gitlab02.uchile.cl/desarrollo-vti/email-api-back/healthcheck"
	"gitlab02.uchile.cl/desarrollo-vti/email-api-back/middleware"
	"go.elastic.co/apm/module/apmgin"
)

// @title           Envia Mail Back
// @version         1.0
// @description     This is a sample server server.
// @termsOfService  http://swagger.io/terms/

// @contact.name   Hugo Cárcamo
// @contact.url    https://mesadeservicios.uchile.cl/otrs/index.pl
// @contact.email  hugo.carcamo@uchile.cl

// @license.name  Apache 2.0
// @license.url   http://www.apache.org/licenses/LICENSE-2.0.html

// @host      local.uchile.cl:8080
// @BasePath  /api/v1
func main() {
	host := os.Getenv("HOST")
	if host == "" {
		host = "local.uchile.cl:8080"
	}
	docs.SwaggerInfo.Host = host
	r := gin.New()
	prefix := "/api/v1"
	r.Use(apmgin.Middleware(r), middleware.Cors(), gin.LoggerWithConfig(gin.LoggerConfig{
		SkipPaths: []string{prefix + "/healthcheck"}}))

	v1NoAuth := r.Group(prefix)
	var h healthcheck.Services
	h.Load("Local", "tcp", "127.0.0.1", 8080)
	v1NoAuth.GET("/healthcheck", h.Healthchecks)
	v1NoAuth.POST("/notificacion", api.Send)

	url := gs.URL(fmt.Sprintf("%s/swagger/doc.json", prefix))
	v1NoAuth.GET("/swagger/*any", gs.WrapHandler(sf.Handler, url))
	//para no mostrar log de check de healthcheck por consola
	//r.Use(gin.LoggerWithConfig(gin.LoggerConfig{SkipPaths: []string{prefix + "/healthcheck"}}))
	r.Run(":8080")
}
